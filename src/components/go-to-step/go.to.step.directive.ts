/// <reference path="../../../typings/tsd.d.ts" />

module app.go.to.step {

    'use strict';


    // ----- directiveFunction -----

    function directiveFunction(): ng.IDirective {

        var directive = {
            restrict: 'E',
            templateUrl: 'components/go-to-step/go-to-step.html',
            scope: {
            },
            controller: 'GoToStepController',
            controllerAs: 'vm',
            link: function(scope, element) {
                angular.element('body>#goToStepModal').remove();
                element.find('#goToStepModal').appendTo('body');
            }
        };

        return directive;
    }


    // ----- ControllerFunction -----

    interface iControllerfunction { };

    class ControllerFunction implements iControllerfunction {
        static $inject = ['$location', '$window', 'CoreFactory', 'api', 'scenarios', 'steps', 'sharedData'];
        scenarios: any;
        selectedScenario: any;
        selectedSteps: any;
        selectedStep: any;
        isStepToBeAdded: boolean;
        isScenarioToBeAdded: boolean;
        newScenario: any;
        newStep: any;
        sortableOption: any;

        constructor(private location, private window, private cf, private baseUrl, private scenarioUrl, private stepUrl, private shared) {
            this.getData();
            var self = this;
            this.sortableOption = {
                stop: function(selectedSteps, droppedIndex) {
                    var updatedSteps = [],
                        updatedStep = {},
                        i = 1;
                    _.each(selectedSteps, function(step) {
                        updatedStep = {};
                        updatedStep['stepId'] = step['stepId'];
                        updatedStep['stepOrder'] = i;
                        updatedSteps.push(updatedStep);
                        i++;
                    });

                    self.cf.update(self.baseUrl + self.stepUrl, JSON.stringify({
                        'steps': updatedSteps
                    }))
                        .then(function() {
                            var i = 1;
                            _.each(self.selectedSteps, function(step) {
                                step['stepOrder'] = i;
                                i++;
                            });
                            self.selectedSteps = _.sortBy(self.selectedSteps, 'stepOrder');
                        },
                        function() {
                            self.selectedSteps = _.sortBy(self.selectedSteps, 'stepOrder');
                        });
                }
            };
        };

        getData = (): void => {
            let self = this;
            this.cf.get(self.baseUrl + self.scenarioUrl)
                .then(function(data) {
                    self.scenarios = data;
                    self.selectedScenario = (data.length > 0) ? data[0].scenarioId : undefined;
                    self.initializeSteps();
                });
        };
        initializeSteps = (): void => {
            let self = this;
            let selectedScenario;
            selectedScenario = _.find(self.scenarios, function(scenario) {
                return (scenario['scenarioId'] === self.selectedScenario);
            });
            self.selectedSteps = _.sortBy(selectedScenario.steps, 'stepOrder');
        };
        updateSelectedScenario = (scenario): void => {
            let self = this;
            self.selectedScenario = scenario.scenarioId;
            self.selectedStep = undefined;
            self.initializeSteps();
        };
        updateSelectedStep = (step): void => {
            let self = this;
            self.selectedStep = step.stepId;
        }
        showStepCreationForm = (): void => {
            let self = this;
            self.newStep = {};
            self.isStepToBeAdded = (self.isStepToBeAdded) ? false : true;
        };
        showScenarioCreationForm = (): void => {
            let self = this;
            self.newScenario = {};
            self.isScenarioToBeAdded = (self.isScenarioToBeAdded) ? false : true;
        };
        addScenario = (): void => {
            let self = this;
            let scenario = self.newScenario;
            scenario['scenarioDescription'] = 'Re-routing all cargos';
            self.cf.set(self.baseUrl + self.scenarioUrl, scenario)
                .then(function(data) {
                    self.scenarios.push(data);
                    self.isScenarioToBeAdded = false;
                    self.shared.setData('scenario', data);
                });
        };
        findNextStepOrder = (): number => {
            let maxStepOrder = 0;
            let self = this;
            let steps = self.selectedSteps;
            _.each(steps, function(step) {
                if(step['stepOrder'] > maxStepOrder) {
                    maxStepOrder = step['stepOrder'];
                }
            })
            return (maxStepOrder + 1);
        };
        updateSelectedScenariosWithNewStep = (data): void => {
            let self = this;
            _.each(self.scenarios, function(scenario) {
                if (scenario['scenarioId'] === self.selectedScenario) {
                    scenario['steps'].push(data);
                }
            });
        };
        addStep = (): void => {
            let self = this;
            let step = self.newStep;
            step.stepScenarioId = self.selectedScenario;
            step.stepOrder = self.findNextStepOrder();
            self.cf.set(self.baseUrl + self.stepUrl, step)
                .then(function(data) {
                    var selectedScenarioData;
                    self.updateSelectedScenariosWithNewStep(data);
                    self.initializeSteps();
                    self.isStepToBeAdded = false;
                    selectedScenarioData = _.find(self.scenarios, function(scenario) {
                        return (scenario['scenarioId'] === self.selectedScenario);
                    });
                    self.shared.setData('scenario', selectedScenarioData);
                });
        };
        goToStep = (): void => {
            var self = this,
                selectedScenarioId = self.selectedScenario,
                selectedStepId = self.selectedStep,
                allSteps = self.selectedSteps,
                scenarios = self.scenarios,
                selectedScenario = _.find(scenarios, function(scenario) {
                    return (scenario['scenarioId'] === selectedScenarioId);
                }),
                stepViewerData = {
                    scenario: selectedScenario,
                    steps: allSteps,
                    selectedStep: selectedStepId
                },
                nextPage = self.location.protocol() + '://' + self.location.host() + ':' + self.location.port() + '/step_viewer';
            localStorage.setItem('stepViewerData', JSON.stringify(stepViewerData));
            self.window.location.href = nextPage;
        };
    };

    angular.module('app.go.to.step')
        .directive('tmplGotostep', directiveFunction)
        .controller('GoToStepController', ControllerFunction);

};
