/// <reference path="../../../typings/tsd.d.ts" />

module app.go.to.step {
    'use strict';

    angular.module('app.go.to.step', [
        'app.core'
    ]);
};
