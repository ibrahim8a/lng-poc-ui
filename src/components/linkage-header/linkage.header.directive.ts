/// <reference path="../../../typings/tsd.d.ts" />

module app.linkage.header {

    'use strict';


    // ----- directiveFunction -----

    function directiveFunction(): ng.IDirective {

        var directive = {
            restrict: 'E',
            templateUrl: 'components/linkage-header/linkage-header.html',
            scope: {
            },
            controller: 'LinkageHeaderController',
            controllerAs: 'vm'
        };

        return directive;
    }


    // ----- ControllerFunction -----

    interface iControllerfunction { };

    class ControllerFunction implements iControllerfunction {
        
        static $inject = ['$location', '$scope', 'sharedData'];

        title: number;
        url: string;
        titleHeader: string;
        isShown: boolean = true;
        buttonText: string = 'Hide Summary';

        constructor(private loc, private scope, private shared) {
            let self = this;

            self.getDetails();

            scope.$on('data_added', function() {
                let data = shared.getData();
                self.isShown = data.summaryVisible;
            });
        };

        getDetails = (): void => {
            let self = this;
            let urls = self.loc.path().replace('_', ' ').split('/');

            self.url = urls[1];
            self.title = urls[2];

            let str = self.url.split(' ')[0];
            self.titleHeader = str.charAt(0).toUpperCase() + str.slice(1);
        };

        toggleSummary = (): void => {
            let self = this;

            self.isShown = !self.isShown;

            self.shared.setData('summaryVisible', self.isShown);
        };

    };

    angular.module('app.linkage.header')
        .directive('tmplLinkageheader', directiveFunction)
        .controller('LinkageHeaderController', ControllerFunction);

};
