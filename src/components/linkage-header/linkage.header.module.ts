/// <reference path="../../../typings/tsd.d.ts" />

module app.linkage.header {
    'use strict';

    angular.module('app.linkage.header', [
        'app.core'
    ]);
};
