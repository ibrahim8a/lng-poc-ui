/// <reference path="../../../typings/tsd.d.ts" />

module app.portfolio.transactions {

    'use strict';


    // ----- directiveFunction -----

    function directiveFunction(): ng.IDirective {

        var directive = {
            restrict: 'E',
            templateUrl: 'components/portfolio-transactions/portfolio-transactions.html',
            scope: {
            },
            controller: 'PortfolioTransactionsController',
            controllerAs: 'vm'
        };
        return directive;
    }


    // ----- ControllerFunction -----

    interface iControllerfunction { };

    class ControllerFunction implements iControllerfunction {

        static $inject = ['CoreFactory', 'api', 'transactionDetails', '$scope', 'sharedData', 'allTransactions'];
        saleData: Object;
        purchaseData: Object;
        shippingData: Object;
        financialData: Object;
        selectedLinkage: any;
        selectedTab = 'sale';
        showSelectedLinkageTransactions: boolean;
        tabs = ['sale', 'purchase', 'shipping', 'financial'];
        gridOptions = {};
        saleColumnDef;
        purchaseColumnDef;
        shippingColumnDef;
        financialColumnDef;
        constructor(private cf, private baseUrl, private transactions, private scope, private shared, private allTransactions) {
            var self = this;
            self.gridOptions['enableFiltering'] = true;
            if(this.selectedLinkage) {
                this.getData();
            }
            scope.$on('data_added', function() {
                var data = shared.getData(),
                    selectedLinkage = data.selectedLinkage;
                self.selectedLinkage = selectedLinkage;
                self.showSelectedLinkageTransactions = true;
                if (selectedLinkage) {
                    self.getData();
                }
            });
        };
        toggleTransactionList = (): void => {
            this.showSelectedLinkageTransactions = !this.showSelectedLinkageTransactions;
            this.getData();
        };
        getData = (): void => {
            let self = this;
            let url;
            if (self.showSelectedLinkageTransactions) {
                url = self.baseUrl + self.transactions + '/' + this.selectedTab + '/' + this.selectedLinkage;
            } else {
                url = self.baseUrl + self.allTransactions + '/' + this.selectedTab;
            }
            this.cf.get(url)
                .then(function(data) {
                    self[self.selectedTab + 'Data'] = data;
                    self.gridOptions = {
                        data: data, 
                        minimumColumnSize: 30, 
                        horizontalScrollThreshold: 10, 
                        enableFiltering: true
                    };
                });
        };
        updateTable = ($evt): void => {
            let currElm: HTMLElement = $evt.currentTarget;
            this.selectedTab = currElm.getAttribute('data-tab');
            this.gridOptions['data'] = [];
            this.getData();
        };
    };

    angular.module('app.portfolio.transactions')
        .directive('tmplPortfoliotransactions', directiveFunction)
        .controller('PortfolioTransactionsController', ControllerFunction);

};
