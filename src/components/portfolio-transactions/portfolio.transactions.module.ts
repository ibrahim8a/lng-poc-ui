/// <reference path="../../../typings/tsd.d.ts" />

module app.portfolio.transactions {
    'use strict';

    angular.module('app.portfolio.transactions', [
        'app.core'
    ]);
};
