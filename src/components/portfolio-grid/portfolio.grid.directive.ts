/// <reference path="../../../typings/tsd.d.ts" />

module app.portfolio.grid {

    'use strict';


    // ----- directiveFunction -----

    function directiveFunction(): ng.IDirective {

        let directive = {
            restrict: 'E',
            templateUrl: 'components/portfolio-grid/portfolio-grid.html',
            scope: {
            },
            controller: 'PortfolioGridController',
            controllerAs: 'vm'
        };

        return directive;
    }


    // ----- ControllerFunction -----

    interface iControllerfunction { };

    class ControllerFunction implements iControllerfunction {
        
        static $inject = ['$scope', '$location', '$timeout', 'CoreFactory', 'sharedData', 'api', 'years', 'linkages', 'linkagesByYear', 'unlinkedTransactions'];

        years: Array<any>;
        tableData: Array<any>;
        data: Array<any>;
        linkages: Array<any>;
        originHeaders: Array<any>;
        destinationHeaders: Array<any>;
        vessels: Array<any>;
        originAndDestination: Array<any>;
        isSaleOriented: boolean = false;
        originModel: Array<any> = [];
        destModel: Array<any> = [];
        linkageModel: Array<any> = [];
        linkage: any;
        selectedLinkageId: any;
        selectedOrigin: any;
        selectedDest: any;
        selectedVessel: any;
        shipingQuantity: any;
        selectedOriginDate: any;
        selectedDestDate: any;
        originDateModel: any;
        destDateModel: any;
        selectedYears: Array<number> = [];
        selectedLinkageData: Object;
        originModalValue: any;
        destModalValue: any;
        pnlData: any;
        toastStatus: string = '';
        toastTitle: string = '';
        toastBody: string = '';
        isToastShown: boolean = false;
        unlikedOrigin: Array<any> = [];
        unlikedDestination: Array<any> = [];
        stepId: number = null;
        scenarioTitle: string = '';
        stepCount: string = '';
        stepName: string = '';
        latestPnl: any;
        pnlChangePercentage: any;
        dropTarget: any;
        dropAttr: any;

        constructor(private scope, private loc, private timeout, private cf, private shared, private api, private yrs, private lnkg, private lby, private unlikedtrans) {
            shared.setData('activeHeader', 'overview');
            this.getPageDetail();
            this.getYear();
            this.getTableData();
            this.getUnlinkedTransactions();
        };

        getPageDetail = () => {
            let self = this;

            let path = self.loc.path();

            if(path.indexOf('step') !== -1) {
                let data = JSON.parse(localStorage.getItem('stepViewerData'));
                self.stepId = parseInt(data.selectedStep);
            }
        }

        getYear = (): void => {
            let self = this;

            let url = self.api + self.yrs;

            if(self.stepId !== null) {
                url += '/' + self.stepId;
            }

            self.cf.get(url)
                .then(function(data) {
                    self.years = _.sortBy(data, function(entry: any) {
                        return entry.year;
                    });

                    self.selectedYears = _.map(self.years, function(entry: any) {
                        return entry.year;
                    });

                    self.shared.setData('years', self.years);
                });
        };

        getYearBasedData = ($evt): void => {
            let self = this;

            let currElm: HTMLElement = $evt.currentTarget.parentElement;
            let value = parseInt(currElm.textContent);
            let isChanged = false;

            if(currElm.classList.contains('active')) {
                if (self.selectedYears.length > 1) {
                    currElm.classList.remove('active');
                    let index = self.selectedYears.indexOf(value);
                    self.selectedYears.splice(index, 1);
                    isChanged = true;
                }
            } else {
                currElm.classList.add('active');
                self.selectedYears.push(value);
                isChanged = true;
            }

            if (isChanged) {
                let postParams: any = {
                    years: self.selectedYears
                };

                if(self.stepId !== null) {
                    postParams.stepId = self.stepId;
                }

                self.cf.set(self.api + self.lby, postParams)
                    .then(function(data) {
                        self.handleLinkageCallback(data);
                    });
            }
        };

        getTableData = (): void => {
            let self = this;
            let url = self.api + self.lby;

            let postParams: any = {};

            if(self.stepId !== null) {
                postParams.stepId = self.stepId;
            }

            self.cf.set(url, postParams)
                .then(function(data) {
                    self.handleLinkageCallback(data);
                });
        };

        handleLinkageCallback = (data: any): void => {
            let self = this;
            let linkages = data.linkages;

            if (linkages && linkages.length > 0) {
                let selectedLinkageData = data.selectedLinkage;
                self.data = linkages;
                self.selectedLinkageData = selectedLinkageData;
                self.shared.setData('totalTransactions', linkages.length);
                self.formatPnLData(selectedLinkageData.pnl, selectedLinkageData.linkage);
                self.processTableData(linkages);
                self.getTableHeaders(linkages);
            }
        }

        processTableData = (data: Array<any>, initLinkage?: any): void => {
            let processedData: any = [];
            let firstLinkage: any;

            if (!this.isSaleOriented) {
                _.forEach(data, function(entry: any, index) {
                    if (processedData.length === 0) {
                        let tempObj: Object = {
                            origin: entry.transactions.purchase.origin,
                            length: 1,
                            entry: [{
                                destination: entry.transactions.sale.destination,
                                length: 1,
                                entry: [entry.linkageId]
                            }]
                        }
                        firstLinkage = entry.linkageId;
                        processedData.push(tempObj);
                    } else {
                        let topindex = _.findIndex(processedData, { origin: entry.transactions.purchase.origin });

                        if (topindex === -1) {
                            processedData.push({
                                origin: entry.transactions.purchase.origin,
                                length: 1,
                                entry: [{
                                    destination: entry.transactions.sale.destination,
                                    length: 1,
                                    entry: [entry.linkageId]
                                }]
                            });
                        } else {
                            let selectedObj: any = processedData[topindex];
                            processedData[topindex].length += 1;
                            let innerIndex = _.findIndex(selectedObj.entry, { destination: entry.transactions.sale.destination });

                            if (innerIndex === -1) {
                                processedData[topindex].entry.push({
                                    destination: entry.transactions.sale.destination,
                                    length: 1,
                                    entry: [entry.linkageId]
                                });
                            } else {
                                processedData[topindex].entry[innerIndex].length += 1;
                                processedData[topindex].entry[innerIndex].entry.push(entry.linkageId);
                            }
                        }
                    }
                });
            } else {
                _.forEach(data, function(entry: any, index) {
                    if (processedData.length === 0) {
                        let tempObj: Object = {
                            origin: entry.transactionsntry.transactions.sale.destination,
                            length: 1,
                            entry: [{
                                destination: entry.transactions.purchase.origin,
                                length: 1,
                                entry: [entry.linkageId]
                            }]
                        }
                        firstLinkage = entry.linkageId;
                        processedData.push(tempObj);
                    } else {
                        let topindex = _.findIndex(processedData, { origin: entry.transactions.sale.destination });

                        if (topindex === -1) {
                            processedData.push({
                                origin: entry.transactions.sale.destination,
                                length: 1,
                                entry: [{
                                    destination: entry.transactions.purchase.origin,
                                    length: 1,
                                    entry: [entry.linkageId]
                                }]
                            });
                        } else {
                            let selectedObj: any = processedData[topindex];
                            processedData[topindex].length += 1;
                            let innerIndex = _.findIndex(selectedObj.entry, { destination: entry.transactions.purchase.origin });

                            if (innerIndex === -1) {
                                processedData[topindex].entry.push({
                                    destination: entry.transactions.purchase.origin,
                                    length: 1,
                                    entry: [entry.linkageId]
                                });
                            } else {
                                processedData[topindex].entry[innerIndex].length += 1;
                                processedData[topindex].entry[innerIndex].entry.push(entry.linkageId);
                            }
                        }
                    }
                });
            }

            if (initLinkage) {
                this.getTransactionDetail(initLinkage);
            } else {
                this.getTransactionDetail(firstLinkage);
            }

            this.tableData = processedData;
        };

        getUnlinkedTransactions = () => {
            let self = this;
            let url = self.api + self.unlikedtrans;
            let unlikedOrigin: Array<any> = [];
            let unlikedDestination: Array<any> = [];

            self.cf.get(url)
                .then(function(data) {
                    _.each(data, function(entry: any) {
                        if (entry.transactionType === 'SALE') {
                            unlikedDestination.push({
                                key: entry.transactionId,
                                value: entry.transactionId + ' | ' + entry.destination
                            });
                        } else if (entry.transactionType === 'PURCHASE') {
                            unlikedOrigin.push({
                                key: entry.transactionId,
                                value: entry.transactionId + ' | ' + entry.origin
                            });
                        }
                    });

                    self.unlikedOrigin = unlikedOrigin;
                    self.unlikedDestination = unlikedDestination;
                });
        };

        getTableHeaders = (data: Array<any>): any => {
            let originHeaders = _.uniq(_.map(data, function(entry: any) {
                return entry.transactions.purchase.origin;
            }));

            let destinationHeaders = _.uniq(_.map(data, function(entry: any) {
                return entry.transactions.sale.destination;
            }));

            let vessels = _.uniq(_.map(data, function(entry: any) {
                return entry.transactions.shipping.vessel;
            }));

            let linkages = _.uniq(_.map(data, function(entry: any) {
                return (entry.linkageId).toString();
            }));

            let originAndDestination = _.uniq(
                _.union(originHeaders, destinationHeaders),
                false,
                function(item, key, a) {
                    return item;
                });

            this.linkages = linkages;
            this.originHeaders = originHeaders;
            this.destinationHeaders = destinationHeaders;
            this.vessels = vessels;
            this.originAndDestination = originAndDestination;
        };

        formatPnLData = (data: any, linkage: any): void => {
            let formattedData = [];
            let rowInFormattedData;
            let date;
            let utcDate;
            let oldPnl;
            let newPnl;
            let changeInPnl;

            for (let i = 0; i < data.length; i++) {
                rowInFormattedData = [];
                date = new Date(data[i].date);
                utcDate = Date.UTC(date.getFullYear(), date.getMonth(), date.getDate());
                rowInFormattedData.push(utcDate);
                rowInFormattedData.push(parseInt(data[i].newPnl));
                formattedData.push(rowInFormattedData);
            }
            this.pnlData = formattedData;
            this.latestPnl = parseFloat(linkage.newPnl);
            oldPnl = parseInt(linkage.oldPnl);
            newPnl = parseInt(linkage.newPnl);
            changeInPnl = ((newPnl - oldPnl)*100)/oldPnl;
            this.pnlChangePercentage = Math.round((changeInPnl) * 100)/100;
        }

        getTransactionDetail = (linkage: any): void => {
            let self = this;

            self.selectedLinkageId = linkage;
            self.shared.setData('selectedLinkage', linkage);
            let length = self.data.length;

            let url = self.api + self.lnkg + '/' + linkage;
            self.cf.get(url)
                .then(function(data) {                 
                    let selectedLinkageData = data;
                    self.selectedLinkageData = selectedLinkageData;
                    self.formatPnLData(data.pnl, data.linkage);
                });

            for (let inc = 0; inc < length; inc++) {
                if(self.data[inc].linkageId === linkage) {
                    let data = self.data[inc];

                    self.linkage = data;
                    self.selectedOrigin = data.transactions.purchase.origin;
                    self.selectedOriginDate = data.transactions.purchase.loadingDate;
                    self.selectedDest = data.transactions.sale.destination;
                    self.selectedDestDate = data.transactions.sale.deliveryDate;
                    self.selectedVessel = data.transactions.shipping.vessel;
                    self.shipingQuantity = data.transactions.shipping.quantity;

                    self.originDateModel.setDate(self.selectedOriginDate);
                    self.destDateModel.setDate(self.selectedDestDate);

                    return;
                }
            }
        };

        originSelectCallback = (values: Array<string>): void => {
            let self = this;

            if(values) {
                self.originModel = values;
            }

            self.processModels();
        };

        destinationSelectCallback = (values: Array<string>): void => {
            let self = this;

            if (values) {
                self.destModel = values;
            }

            self.processModels();
        };

        linkageSelectCallback = (values: Array<string>): void => {
            let self = this;

            if (values) {
                self.linkageModel = values;
            }

            self.processModels();
        };

        processModels = (): void => {
            let self = this;

            if(self.originModel.length === 0) {
                self.originModel = self.originHeaders;
            }

            if(self.destModel.length === 0) {
                self.destModel = self.destinationHeaders;
            }

            if(self.linkageModel.length === 0) {
                self.linkageModel = self.linkages;
            }

            let filteredData = self.data;

            filteredData = _.filter(filteredData, function(entry) {
                return self.originModel.indexOf(entry.transactions.purchase.origin) !== -1;
            });

            filteredData = _.filter(filteredData, function(entry) {
                return self.destModel.indexOf(entry.transactions.sale.destination) !== -1;
            });

            filteredData = _.filter(filteredData, function(entry) {
                return self.linkageModel.indexOf((entry.linkageId).toString()) !== -1;
            });

            self.processTableData(filteredData);
        }

        setTransactionOrigin = (value: any): void => {
            this.selectedOrigin = value;
        };

        setTransactionDest = (value: any): void => {
            this.selectedDest = value;
        };

        setVessel = (value: any): void => {
            this.selectedVessel = value;
        };

        resetLinkageChanges = (): void => {
            let self = this;

            self.selectedOrigin = self.linkage.transactions.purchase.origin;
            self.selectedOriginDate = self.linkage.transactions.purchase.loadingDate;
            self.selectedDest = self.linkage.transactions.sale.destination;
            self.selectedDestDate = self.linkage.transactions.sale.deliveryDate;
            self.selectedVessel = self.linkage.transactions.shipping.vessel;
            self.shipingQuantity = self.linkage.transactions.shipping.quantity;
        };

        onOriginDateUpdate = (pikaday: any): void => {
            let str = pikaday.toString();
            let date = new Date(str);
            this.selectedOriginDate = date.toISOString();
        };

        onDestDateUpdate = (pikaday: any): void => {
            let str = pikaday.toString();
            let date = new Date(str);
            this.selectedDestDate = date.toISOString();
        };

        updateLinkage = (param?: any): void => {
            let self = this;
            let linkage: any = self.linkage;
            let postUrl = self.api + self.lnkg + '/' + self.selectedLinkageId;

            linkage.transactions.purchase.origin = self.selectedOrigin;
            linkage.transactions.purchase.loadingDate = self.selectedOriginDate;
            linkage.transactions.sale.destination = self.selectedDest;
            linkage.transactions.sale.deliveryDate = self.selectedDestDate;
            linkage.transactions.shipping.vessel = self.selectedVessel;
            linkage.transactions.shipping.quantity = self.shipingQuantity;

            if(param) {
                if(param.key === 'origin') {
                    linkage.transactions.purchase.origin = param.value;
                } else {
                    linkage.transactions.sale.destination = param.value;
                }
            }

            // Removing validation for now
            /*if (linkage.transactions.purchase.origin === linkage.transactions.sale.destination) {
                self.showToast('warn', 'Warning', 'Origin and Destination cannot be same.');
                return;
            }*/

            self.cf.update(postUrl, linkage)
                .then(function(data) {
                    if (data.status) {
                        _.map(self.data, function(entry) {
                            if (entry.linkageId === self.selectedLinkageId) {
                                let index = self.data.indexOf(entry);
                                self.data[index] = linkage;
                                self.processTableData(self.data, linkage.linkageId);
                            }
                        });

                        self.showToast('success', 'Success', 'Linkage successfully updated');
                    } else {
                        self.showToast('error', 'Error', 'Server is not responding. Please try again later.');
                    }
                }, function() {
                    self.showToast('error', 'Error', 'Unable to update Table data. Please try again later.');
                });
        };

        updateModalOrigin = (value: any): void => {
            this.originModalValue = value;
        };

        updateModalDest = (value: any): void => {
            this.destModalValue = value;
        };

        createLinkage = (): void => {
            let self = this;
            let postUrl = self.api + self.lnkg;

            let originValue = self.originModalValue;
            let destValue = self.destModalValue;

            let isValid = originValue && originValue.length > 0 && destValue && destValue.length > 0;

            if (isValid) {
                let transactionsData: any = {
                    transactions: [originValue[0].key, destValue[0].key]
                };

                if (self.stepId !== null) {
                    transactionsData.stepId = self.stepId
                }

                self.cf.set(postUrl, transactionsData)
                    .then(function(data) {
                        if(data.status) {
                            let newLinkageId = data.linkage.linkageId;

                            self.linkages.push(newLinkageId.toString());
                            self.selectedLinkageId = newLinkageId;
                            self.data.push(data.linkage);

                            let index = self.data.indexOf(data.linkage);
                            let currentLinkage = self.data[index];
                            self.processTableData(self.data, currentLinkage.linkageId);

                            self.showToast('success', 'Success', 'Successfully created new linkage');
                            self.shared.setData('selectedLinkage', newLinkageId);
                        } else {
                            self.showToast('error', 'Error', 'Server is not responding. Please try again later.');
                        }
                    }, function() {
                        self.showToast('error', 'Error', 'Unable to update Table data. Please try again later.');
                    });
            } else {
                event.stopPropagation();

                self.showToast('warn', 'Warning', 'Please select both Source and Destination and ensure they are different.');
            }
        };

        onDrop = (target: any, attr: any): void => {
            this.dropTarget = target;
            this.dropAttr = attr;
        }

        onDropSuccess = (source: any): void => {
            let self = this;

            self.selectedLinkageId = source;
            let length = self.data.length;
            let validated = false;
            let msg = 'Origin and Destination cannot be same.';
            let params: Object = {};

            for (let inc = 0; inc < length; inc++) {
                if (self.data[inc].linkageId === source) {
                    let data = self.data[inc];
                    self.linkage = data;
                    validated = true;

                    // Removing any kind of validation
                    /*if (self.dropAttr === 'origin' && data.transactions.sale.destination !== self.dropTarget) {
                        if (data.transactions.purchase.origin === self.dropTarget) {
                            msg = 'You cannot drop linkage to same origin';
                        } else {
                            validated = true;
                            self.linkage = data;
                        }
                    }

                    if (self.dropAttr === 'destination' && data.transactions.purchase.origin !== self.dropTarget) {
                        if (data.transactions.sale.destination === self.dropTarget) {
                            msg = 'You cannot drop linkage to same destination';
                        } else {
                            validated = true;
                            self.linkage = data;
                        }
                    }*/
                }
            }

            if (validated) {
                self.selectedOrigin = self.linkage.transactions.purchase.origin;
                self.selectedOriginDate = self.linkage.transactions.purchase.loadingDate;
                self.selectedDest = self.linkage.transactions.sale.destination;
                self.selectedDestDate = self.linkage.transactions.sale.deliveryDate;
                self.selectedVessel = self.linkage.transactions.shipping.vessel;
                self.shipingQuantity = self.linkage.transactions.shipping.quantity;

                if (self.dropAttr === 'origin') {
                    params = {
                        key: 'origin',
                        value: self.dropTarget
                    }
                } else {
                    params = {
                        key: 'destination',
                        value: self.dropTarget
                    }
                }

                self.updateLinkage(params);
            } else {
                self.showToast('warn', 'Warning', msg);
            }
        };

        showToast = (status, title, message, willHide = true) => {
            let self = this;

            self.toastStatus = status;
            self.toastTitle = title;
            self.toastBody = message;
            self.isToastShown = true;

            if (willHide) {
                self.hideToast();
            }
        };

        hideToast = (): void => {
            var self = this;

            self.timeout(function() {
                self.isToastShown = false;
            }, 3000);
        };

    };

    angular.module('app.portfolio.grid')
        .directive('tmplPortfolio', directiveFunction)
        .controller('PortfolioGridController', ControllerFunction);

};
