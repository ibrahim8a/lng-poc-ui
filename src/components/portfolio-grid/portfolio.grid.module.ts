/// <reference path="../../../typings/tsd.d.ts" />

module app.portfolio.grid {
    'use strict';

    angular.module('app.portfolio.grid', [
        'app.core'
    ]);
};
