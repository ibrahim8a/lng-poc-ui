/// <reference path="../../../typings/tsd.d.ts" />

module app.portfolio.updates {

    'use strict';


    // ----- directiveFunction -----

    function directiveFunction(): ng.IDirective {

        var directive = {
            restrict: 'E',
            templateUrl: 'components/portfolio-updates/portfolio-updates.html',
            scope: {
            },
            controller: 'PortfolioUpdatesController',
            controllerAs: 'vm'
        };

        return directive;
    }


    // ----- ControllerFunction -----

    interface iControllerfunction { };

    class ControllerFunction implements iControllerfunction {
        static $inject = ['$location', '$window', '$scope', 'CoreFactory', 'api', 'scenarios', 'sharedData'];
        scenarioData: any;
        scenario: Object;
        
        constructor(private location, private window, private scope, private cf, private baseUrl, private scenarioUrl, private shared) {
            shared.setData('activeHeader', 'updates');
            this.getData();
            var self = this;
            scope.$on('data_added', function() {
                let data = shared.getData();
                let scenario = data.scenario;
                let isScenarioPresent;
                if(scenario) {
                    isScenarioPresent = _.find(self.scenarioData, function(scenarioAtIndex) {
                        return (scenarioAtIndex['scenarioId'] === scenario.scenarioId);
                    });
                    if(!isScenarioPresent) {
                        self.scenarioData.push(scenario);
                    } else {
                        _.each(self.scenarioData, function(scenarioAtIndex) {
                            if (scenarioAtIndex['scenarioId'] === scenario.scenarioId) {
                                _.extend(scenarioAtIndex, scenario);
                            }
                        });
                    }
                }
            });
        };
        
        getData = (): void => {
            let self = this;
            this.cf.get(self.baseUrl + self.scenarioUrl)
                .then(function(data) {
                    self.scenarioData = data;
                });
        };

        goToScenarioDetailPage = ($evt): void => {
            let currElm: HTMLElement = $evt.currentTarget;
            let scenarioId = parseInt(currElm.getAttribute('data-scenario-id'));
            let nextPage = this.location.protocol() + '://' + this.location.host() + ':' + this.location.port() + '/scenario_details/' + scenarioId;
            this.window.location.href = nextPage;
        };

        createScenario = (): void => {
            let self = this;
            let scenario = self.scenario;
            self.cf.set(self.baseUrl + self.scenarioUrl, scenario)
                .then(function(data) {
                    self.scenarioData.push(data);
                });
        };
    };

    angular.module('app.portfolio.updates')
        .directive('tmplPortfolioupdates', directiveFunction)
        .controller('PortfolioUpdatesController', ControllerFunction);

};
