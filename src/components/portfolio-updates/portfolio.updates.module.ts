/// <reference path="../../../typings/tsd.d.ts" />

module app.portfolio.updates {
    'use strict';

    angular.module('app.portfolio.updates', [
        'app.core'
    ]);
};
