/// <reference path="../../../typings/tsd.d.ts" />

module app.topnav {
    'use strict';


    // ----- directiveFunction -----

    function directiveFunction(): ng.IDirective {

        var directive = {
            restrict: 'E',
            templateUrl: 'components/topnav/topnav.html',
            scope: {
            },
            controller: 'TopnavController',
            controllerAs: 'vm'
        };

        return directive;
    }

    // ----- ControllerFunction -----

    interface iControllerfunction { };

    class ControllerFunction implements iControllerfunction {
        static $inject = ['$scope', 'sharedData'];

        name: string;
        activeHeader: string;

        constructor(private scope, private shared) {
            let self = this;
            self.name = 'Bikas';

            scope.$on('data_added', function() {
                let data = shared.getData();
                self.activeHeader = data.activeHeader;
            });
        };
    };

    angular
        .module('app.topnav')
        .directive('tmplTopnav', directiveFunction)
        .controller('TopnavController', ControllerFunction);

};
