/// <reference path="../../../typings/tsd.d.ts" />

module app.scenario.details {
    'use strict';

    angular.module('app.scenario.details', [
        'app.core'
    ]);
};
