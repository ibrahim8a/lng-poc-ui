/// <reference path="../../../typings/tsd.d.ts" />

module app.scenario.details {

    'use strict';


    // ----- directiveFunction -----

    function directiveFunction(): ng.IDirective {

        var directive = {
            restrict: 'E',
            templateUrl: 'components/scenario-details/scenario-details.html',
            scope: {
            },
            controller: 'ScenarioDetailsController',
            controllerAs: 'vm'
        };

        return directive;
    }


    // ----- ControllerFunction -----

    interface iControllerfunction { };

    class ControllerFunction implements iControllerfunction {
        static $inject = ['$location', '$window', '$scope', 'CoreFactory', 'api', 'scenarios', 'steps', 'linkages', 'years', 'unlinkedTransactions', 'sharedData'];
        scenarioData: any;
        step: Object;
        linkageIds: any;
        transactionIds: any;
        
        constructor(private location, private window, private scope, private cf, private baseUrl, private scenarioUrl, private stepUrl, private linkages, private years, private unlinkedTransactions, private shared) {
            shared.setData('activeHeader', 'updates');
            this.getData();
            var self = this;
            scope.$on('data_added', function() {
                let data = shared.getData();
                let scenario = data.scenario;
                let isCurrentScenario;
                let newStep;
                if (scenario) {
                    isCurrentScenario = (self.scenarioData.scenarioId === scenario.scenarioId);
                    if (isCurrentScenario) {
                        newStep = self.getAddedStep(self.scenarioData.steps, scenario.steps);
                        self.scenarioData.steps.push(newStep);
                        self.step['stepOrder'] = self.step['stepOrder'] + 1;
                    }
                }
            });
        };
        getAddedStep = (currentSteps, newSteps): Object => {
            var isStepExists;
            for (var i = 0; i < newSteps.length; i++) {
                isStepExists = _.find(currentSteps, function(step) {
                    return step['stepId'] === newSteps[i].stepId;
                });
                if(!isStepExists) {
                    break;
                }
            }
            return newSteps[i];
        };
        getData = (): void => {
            let self = this;
            let urlParams = self.location.path().split('/');
            let paramsLength = urlParams.length;
            let scenarioId = parseInt(urlParams[paramsLength - 1]);
            
            this.cf.get(self.baseUrl + self.scenarioUrl + '/' +scenarioId + self.stepUrl)
                .then(function(data) {
                    self.scenarioData = data;
                    self.scenarioData.steps = _.sortBy(self.scenarioData.steps, 'stepOrder');
                    self.setNextStepOrder(data);
                });
            this.cf.set(self.baseUrl + self.linkages + self.years, {})
                .then(function(data) {
                    self.createLinkageMultiSelectOptions(data);
                });
            this.cf.get(self.baseUrl + self.unlinkedTransactions)
                .then(function(data) {
                    self.createTransactionMultiSelectOptions(data);
                });
        };
        setNextStepOrder = (data): void => {
            let self = this;
            let steps = data.steps;
            let maxStepOrder = 0;
            self.step = {};
            self.step['stepScenarioId'] = data.scenarioId;

            for (var i = 0; i < steps.length; i++) {
                if (steps[i].stepOrder > maxStepOrder) {
                    maxStepOrder = steps[i].stepOrder;
                }
            }
            self.step['stepOrder'] = maxStepOrder + 1;
        };
        createLinkageMultiSelectOptions = (data): void => {
            var self = this,
                parsedLinkages = [],
                linkage = {},
                linkages = data.linkages,
                sale,
                purchase,
                linkageId;
            for (var i = 0; i < linkages.length; i++) {
                linkageId = linkages[i].linkageId;
                sale = linkages[i].transactions.sale.destination;
                purchase = linkages[i].transactions.purchase.origin;
                linkage = {}
                linkage['key'] = linkageId;
                linkage['value'] = purchase + ' | ' + linkageId + ' | ' + sale;
                parsedLinkages.push(linkage);
            }
            self.linkageIds = parsedLinkages;
        };
        createTransactionMultiSelectOptions = (data): void => {
            var self = this,
                parsedTransactions = [],
                transaction = {},
                transactions = data,
                transactionId,
                transactionType,
                value;
            for (var i = 0; i < transactions.length; i++) {
                transactionId = transactions[i].transactionId;
                transactionType = transactions[i].transactionType;
                if (transactionType === 'SALE') {
                    value = transactions[i].destination;
                } else if (transactionType === 'PURCHASE') {
                    value = transactions[i].origin;
                } else if (transactionType === 'SHIPPING') {
                    value = transactions[i].vessel
                } else if (transactionType === 'FINANCIAL') {
                    value = transactions[i].costType
                }
                transaction = {}
                transaction['key'] = transactionId;
                transaction['value'] = transactionType + ' | ' + transactionId + ' | ' + value;
                parsedTransactions.push(transaction);
            }
            self.transactionIds = parsedTransactions;
        };
        addStepToScenario = (): void => {
            let self = this;
            let step = self.step;
            self.cf.set(self.baseUrl + self.stepUrl, step)
                .then(function(data) {
                    self.scenarioData.steps.push(data);
                    self.step['stepOrder'] = data['stepOrder'] + 1;
                });
        }
        linkageSelectCallback = (values: Array<string>): void => {
            let self = this;
            if (values) {
                self.step['linkages'] = _.pluck(values, 'key');
            }
        };
        transactionSelectCallback = (values: Array<string>): void => {
            let self = this;
            if (values) {
                self.step['transactions'] = _.pluck(values, 'key');
            }
        };
        goToStep = (data): void => {
            let self = this,
                selectedScenario = self.scenarioData,
                allSteps = selectedScenario.steps,
                selectedStepId = data.stepId,
                stepViewerData = {
                    scenario: selectedScenario,
                    steps: allSteps,
                    selectedStep: selectedStepId
                },
                nextPage = self.location.protocol() + '://' + self.location.host() + ':' + self.location.port() + '/step_viewer';
            localStorage.setItem('stepViewerData', JSON.stringify(stepViewerData));
            self.window.location.href = nextPage;
        };
    };

    angular.module('app.scenario.details')
        .directive('tmplScenariodetails', directiveFunction)
        .controller('ScenarioDetailsController', ControllerFunction);

};
