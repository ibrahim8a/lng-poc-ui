/// <reference path="../../../typings/tsd.d.ts" />

module app.linkage.details {
    'use strict';

    angular.module('app.linkage.details', [
        'app.core'
    ]);
};
