/// <reference path="../../../typings/tsd.d.ts" />

module app.linkage.details {

    'use strict';


    // ----- directiveFunction -----

    function directiveFunction(): ng.IDirective {

        let directive = {
            restrict: 'E',
            templateUrl: 'components/linkage-details/linkage-details.html',
            scope: {
            },
            controller: 'LinkageDetailsController',
            controllerAs: 'vm'
        };

        return directive;
    }


    // ----- ControllerFunction -----

    interface iControllerfunction { };

    class ControllerFunction implements iControllerfunction {
        static $inject = ['$location', '$scope', 'CoreFactory', 'sharedData', 'api', 'linkages', 'transactions', 'completeTransactions'];

        data: Array<any> = [];
        purchase: any;
        sale: any;
        shipping: any;
        pnl: Array<any> = [];
        risk: Array<any> = [];
        changelogs: Array<any> = [];
        purchaseHistory: Array<any> = [];
        purchaseAmendments: Array<any> = [];
        saleHistory: Array<any> = [];
        saleAmendments: Array<any> = [];
        shippingHistory: Array<any> = [];
        shippingAmendments: Array<any> = [];
        activeAmendment: Array<any> = [];
        activeType: string;
        linkageTable: Array<any> = [];
        purchaseHistoryLength: number = 0;
        saleHistoryLength: number = 0;
        shippingHistoryLength: number = 0;
        changelogLength: number = 0;
        buttonText: string = 'Hide Summary';
        isShown: boolean = true;
        activeTab: number = 0;
        isTransaction: boolean = false;
        isPurchaseShown: boolean = false;
        isSaleShown: boolean = false;
        isShipShown: boolean = false;
        pnlData: any;
        newPnl: number;
        activeRow: number = 0;

        constructor(private loc, private scope,  private cf, private shared, private api, private lnkg, private transactions, private trans) {
            let self = this;
            self.getTransactions();

            scope.$on('data_added', function() {
                let data = shared.getData();
                self.isShown = data.summaryVisible;
            });
        };

        getTransactions = (): void => {
            let self = this;

            let endpoints = self.loc.path().split('/');
            let epLength = endpoints.length;
            let endpoint = endpoints[epLength - 2];
            let requestId = endpoints[epLength - 1];

            let url = self.api;

            if (endpoint.indexOf('linkage') !== -1) {
                url += self.lnkg + '/' + requestId + self.trans;
            } else {
                self.isTransaction = true;
                url += self.transactions + '/' + requestId + self.trans;
            }

            self.cf.get(url)
                .then(function(data) {
                    self.data = data;
                    self.purchase = data.purchase;
                    self.sale = data.sale;
                    self.shipping = data.shipping;
                    self.processRisk(data.risk);
                    self.formatPnLData(data.pnl);
                    self.processChangelog(data);
                    self.handleData();
                });
        };

        formatPnLData = (data: any): void => {
            let formattedData = [];
            let rowInFormattedData;
            let date;
            let utcDate;

            for (let i = 0; i < data.length; i++) {
                rowInFormattedData = [];
                date = new Date(data[i].date);
                utcDate = Date.UTC(date.getFullYear(), date.getMonth(), date.getDate());
                rowInFormattedData.push(utcDate);
                rowInFormattedData.push(parseInt(data[i].newPnl));
                formattedData.push(rowInFormattedData);
            }

            this.pnlData = formattedData;
            if (data && data.length) {
                this.newPnl = data[0].newPnl;
            }
        };

        processChangelog = (data: any): void => {
            let self = this;

            if (data && data.changelogs) {
                self.changelogLength = data.changelogs.length;
                self.changelogs = self.setMinRows(data.changelogs, 6);
            } else {
                self.changelogs = self.setMinRows([], 6);
            }
        }

        processRisk = (data: Array<any>): void => {
            let self = this;

            if (data && data.length > 0) {
                _.each(data, function(entry) {
                    var obj: any = self.getPositiveNegativeValue(entry.risk, entry.previousRisk);

                    entry.variation = obj.variation;
                    entry.variationClass = obj.variationClass;
                    entry.unit = '(' + entry.unit + ')';
                });

                self.setMinRows(data, 6);
            } else {
                self.setMinRows([], 6);
            }

            self.risk = data;
        };

        getPositiveNegativeValue = (firstVal: number, secondVal: number): Object => {
            let variation = firstVal - secondVal;
            let variationStr = '' + variation.toFixed(2);
            let variationClass = 'negative-text';
            if (variation > 0) {
                variationStr = '+' + variationStr;
                variationClass = 'positive-text';
            }

            return {
                variation: variationStr,
                variationClass: variationClass
            };
        };

        handleData = (): void => {
            let self = this;

            let purchaseData: any = self.processData(self.purchase.amendments);
            if (purchaseData) {
                self.purchaseHistory = purchaseData.history;
                self.purchaseHistoryLength = purchaseData.history.length;
                self.purchaseAmendments = purchaseData.amendments;

                self.isPurchaseShown = true;
            }

            if (self.purchase.transactionId) {
                self.linkageTable.push({
                    type: 'Purchase',
                    date: self.purchase.loadingDate,
                    status: self.purchase.status,
                    validated: self.purchase.validated.toUpperCase(),
                    instrumentType: self.purchase.instrumentType,
                    entity: self.purchase.entity.toUpperCase(),
                    portfolio: self.purchase.portfolio.toUpperCase(),
                    thirdParty: self.purchase.thirdParty.toUpperCase()
                });
            }

            if (self.purchase.transactionId !== null) {
                self.isPurchaseShown = true;
            }

            self.purchaseHistory = self.setMinRows(self.purchaseHistory, 4);

            let saleData: any = self.processData(self.sale.amendments);
            if (saleData) {
                self.saleHistory = saleData.history;
                self.saleHistoryLength = saleData.history.length;
                self.saleAmendments = saleData.amendments;

                self.isSaleShown = true;
            }

            if (self.sale.transactionId) {
                self.linkageTable.push({
                    type: 'Sale',
                    date: self.sale.deliveryDate,
                    status: self.sale.status,
                    validated: self.sale.validated.toUpperCase(),
                    instrumentType: self.sale.instrumentType,
                    entity: self.sale.entity.toUpperCase(),
                    portfolio: self.sale.portfolio.toUpperCase(),
                    thirdParty: self.sale.thirdParty.toUpperCase()
                });
            }

            if (self.sale.transactionId !== null) {
                self.isSaleShown = true;
            }

            self.saleHistory = self.setMinRows(self.saleHistory, 4);

            let shipData: any = self.processData(self.shipping.amendments);
            if (shipData) {
                self.shippingHistory = shipData.history;
                self.shippingHistoryLength = shipData.history.length;
                self.shippingAmendments = shipData.amendments;

                self.isShipShown = true;
            }

            if (self.shipping.transactionId) {
                self.linkageTable.push({
                    type: 'Shipping',
                    date: self.shipping.shippingDate,
                    status: self.shipping.status,
                    validated: self.shipping.validated.toUpperCase(),
                    instrumentType: self.shipping.instrumentType,
                    entity: self.shipping.entity.toUpperCase(),
                    portfolio: self.shipping.portfolio.toUpperCase(),
                    thirdParty: self.purchase.thirdParty.toUpperCase()
                });
            }

            if (self.shipping.transactionId !== null) {
                self.isShipShown = true;
            }

            self.shippingHistory = self.setMinRows(self.shippingHistory, 4);

            if (self.purchaseAmendments && self.purchaseAmendments.length > 0) {
                self.activeAmendment = self.purchaseAmendments;
                self.activeType = 'Purchase';
            } else if(self.saleAmendments && self.saleAmendments.length > 0) {
                self.activeAmendment = self.saleAmendments;
                self.activeType = 'Sale';
            } else {
                self.activeAmendment = self.shippingAmendments;
                self.activeType = 'Shipping';
            }
        };

        processData = (amendments: Array<any>): Object => {
            let self = this;

            let amendmentsArr = [];
            let history = [];
            let values = ['origin', 'loadingDate', 'destination', 'deliveryDate', 'vessel', 'quantity', 'qunatityUnit'];

            if (amendments && amendments.length > 0) {
                amendments.forEach(function(entry, ii) {
                    let id = entry.transactionAmendmentId;

                    let amendmentRows = {
                        objectType: entry.objectType,
                        objectReference: entry.objectReference,
                        stateBefore: entry.stateBefore,
                        stateAfter: entry.stateAfter,
                        subObjectType: entry.subObjectType,
                        subObjectReference: entry.subObjectReference,
                        action: entry.action,
                        propertyName: entry.propertyName,
                        displayName: entry.displayName,
                        oldValue: entry.oldValue,
                        newValue: entry.newValue,
                    };

                    let index = _.findIndex(amendmentsArr, function(data) {
                        return data.id === id;
                    });

                    if (index === -1) {
                        let obj:any = self.getPositiveNegativeValue(entry.newPnl, entry.oldPnl);

                        let amendmentDetail = {
                            department: entry.department,
                            departmentClass: entry.department.toLowerCase().replace(' ', '-'),
                            action: entry.action,
                            date: entry.date,
                            user: entry.user,
                            terminalId: entry.terminalId,
                            pnl: obj.variation,
                            pnlClass: obj.variationClass,
                            risk: entry.newRisk || self.getRandomRisk(),
                        };

                        amendmentsArr.push({
                            id: id,
                            amendmentDetail: amendmentDetail,
                            amendmentRows: [amendmentRows]
                        });

                        if (values.indexOf(entry.propertyName) !== -1) {
                            history.push({
                                [entry.propertyName]: entry.newValue
                            });
                        }
                    } else {
                        amendmentsArr[index].amendmentRows.push(amendmentRows);
                        
                        if (values.indexOf(entry.propertyName) !== -1) {
                            history[index][entry.propertyName] = entry.newValue;
                        }
                    }
                });

                return {
                    amendments: amendmentsArr,
                    history: history
                };
            }
        };

        getRandomRisk = (): string => {
            let riskStr = '';

            function generateRandom(): string {
                let max = 10;
                let min = -10;

                let number = Math.random() * (max - min) + min;

                return number.toFixed(1);
            }

            riskStr = generateRandom() + '/' + generateRandom() + '/' + generateRandom() + '/' + generateRandom();

            return riskStr;
        };

        setMinRows = (data: Array<any>, rows: number): Array<any> => {
            if (data.length < rows) {
                let length = rows - data.length;
                for (let ii = 0; ii < length; ii++) {
                    data.push({});
                }
            }

            return data;
        };

        toggleSummary = (): void => {
            let self = this;

            self.isShown = !self.isShown;

            self.shared.setData('summaryVisible', self.isShown);
        };

        setAmendments = (type: string, index: number): void => {
            let self = this;

            if(type === 'Purchase') {
                self.activeAmendment = self.purchaseAmendments;
            } else if(type === 'Sale') {
                self.activeAmendment = self.saleAmendments;
            } else if(type === 'Shipping') {
                self.activeAmendment = self.shippingAmendments;
            }

            self.activeRow = index;

            self.activeType = type;
            self.activeTab = 0;
        };

        setActiveTab = (tab: number): void => {
            this.activeTab = tab;
        };
    };

    angular.module('app.linkage.details')
        .directive('tmplLinkagedetails', directiveFunction)
        .controller('LinkageDetailsController', ControllerFunction);

};
