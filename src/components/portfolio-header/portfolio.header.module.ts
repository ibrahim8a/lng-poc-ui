/// <reference path="../../../typings/tsd.d.ts" />

module app.portfolio.header {
    'use strict';

    angular.module('app.portfolio.header', [
        'app.core'
    ]);
};
