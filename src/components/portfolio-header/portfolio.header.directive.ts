/// <reference path="../../../typings/tsd.d.ts" />

module app.portfolio.header {

    'use strict';


    // ----- directiveFunction -----

    function directiveFunction(): ng.IDirective {

        var directive = {
            restrict: 'E',
            templateUrl: 'components/portfolio-header/portfolio-header.html',
            scope: {
            },
            controller: 'PortfolioHeaderController',
            controllerAs: 'vm'
        };

        return directive;
    }


    // ----- ControllerFunction -----

    interface iControllerfunction { };

    class ControllerFunction implements iControllerfunction {
        
        static $inject = ['$scope', 'sharedData'];

        totalTransactions: number = 0;
        yearsText: string = '';

        constructor(private scope, private shared) {
            this.retreiveData();
        };

        retreiveData = () => {
            let self = this;

            self.scope.$on('data_added', function() {
                let yearLabel: string = '';
                let data = self.shared.getData();

                self.totalTransactions = data.totalTransactions;

                let years = _.map(data.years, function(entry: any) {
                    return entry.year;
                });

                if(years.length === 1) {
                    yearLabel = years[0];
                } else {
                    yearLabel = years[0] + ' to ' + years[years.length - 1];
                }

                self.yearsText = yearLabel;
            });
        }

    };

    angular.module('app.portfolio.header')
        .directive('tmplPortfolioheader', directiveFunction)
        .controller('PortfolioHeaderController', ControllerFunction);

};
