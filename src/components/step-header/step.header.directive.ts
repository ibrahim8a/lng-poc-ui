/// <reference path="../../../typings/tsd.d.ts" />

module app.step.header {

    'use strict';


    // ----- directiveFunction -----

    function directiveFunction(): ng.IDirective {

        var directive = {
            restrict: 'E',
            templateUrl: 'components/step-header/step-header.html',
            scope: {
            },
            controller: 'StepHeaderController',
            controllerAs: 'vm'
        };

        return directive;
    }


    // ----- ControllerFunction -----

    interface iControllerfunction { };

    class ControllerFunction implements iControllerfunction {
        
        static $inject = ['$scope', '$window', 'sharedData'];

        stepId: number;
        stepOrder: number;
        scenarioTitle: string = '';
        stepCount: string = '';
        stepName: string = '';
        data: any;
        steps: Array<any>;
        leftDisabled: boolean = false;
        rightDisabled: boolean = false;

        constructor(private scope, private window, private shared) {
            this.watchSteps();
            this.retreiveData();
        };

        watchSteps = () => {
            let self = this;

            self.scope.$on('data_added', function() {
                let dynData = self.shared.getData().scenario;

                if (dynData && dynData.steps && dynData.steps.length > 0) {
                    let steps = dynData.steps;
                    let localStorageData = JSON.parse(localStorage.getItem('stepViewerData'));
                    let dynId = dynData.scenarioId;
                    let storedId = localStorageData.scenario.scenarioId;

                    if (storedId === dynId) {
                        localStorageData.steps = steps;
                        localStorageData.scenario.steps = steps;

                        localStorage.setItem('stepViewerData', JSON.stringify(localStorageData));

                        self.retreiveData();
                    }
                }
            });
        };

        retreiveData = () => {
            let self = this;
            let steps = [];

            self.data = JSON.parse(localStorage.getItem('stepViewerData'));
            self.stepId = parseInt(self.data.selectedStep);

            self.scenarioTitle = self.data.scenario.scenarioName;
            self.stepCount = self.data.steps.length;

            _.each(self.data.steps, function(entry: any) {
                steps.push(entry.stepId);

                if (self.stepId === entry.stepId) {
                    self.stepName = entry.stepName;
                    self.stepOrder = parseInt(entry.stepOrder);
                }
            });

            self.steps = steps;
            let length = steps.length;

            if(steps.indexOf(self.stepId) === 0) {
                self.leftDisabled = true;
            }

            if(steps.indexOf(self.stepId) === (length - 1)) {
                self.rightDisabled = true;
            } else {
                self.rightDisabled = false;
            }
        };

        selectPrevious = (): void => {
            let self = this;

            let index = self.steps.indexOf(self.stepId);

            self.stepId = self.steps[index - 1];
            self.goToStep();
        };

        selectNext = (): void => {
            let self = this;

            let index = self.steps.indexOf(self.stepId);

            self.stepId = self.steps[index + 1];
            self.goToStep();
        };

        goToStep = (): void => {
            let self = this;
            let selectedStepId = self.stepId;
            let allSteps = self.data.steps;
            let scenarios = self.data.scenario;

            let stepViewerData = {
                scenario: scenarios,
                steps: allSteps,
                selectedStep: selectedStepId
            };

            localStorage.setItem('stepViewerData', JSON.stringify(stepViewerData));
            self.window.location.reload();
        };

    };

    angular.module('app.step.header')
        .directive('tmplStepheader', directiveFunction)
        .controller('StepHeaderController', ControllerFunction);

};
