/// <reference path="../../../typings/tsd.d.ts" />

module app.step.header {
    'use strict';

    angular.module('app.step.header', [
        'app.core'
    ]);
};
