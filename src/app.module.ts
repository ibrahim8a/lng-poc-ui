/// <reference path="../typings/tsd.d.ts" />

module app {
    'use strict';

    angular.module('app', [
        // Common (everybody has access to these)
        'app.core',

        // Features (listed alphabetically)
        'app.approot',
        'app.linkage.details',
        'app.linkage.header',
        'app.portfolio.grid',
        'app.portfolio.transactions',
        'app.portfolio.header',
        'app.topnav',
        'app.portfolio.updates',
        'app.scenario.details',
        'app.step.header',
        'app.go.to.step'
    ]);
};
