/// <reference path="../../typings/tsd.d.ts" />

module app.core {
    'use strict';

    angular
        .module('app.core')
        .factory('sharedData', sharedData);

    sharedData.$inject = ['$rootScope'];

    /* @ngInject */
    function sharedData(rs) {
        let data: Object = {};

        let dataFunctions = {
            data: data,
            getData: getData,
            setData: setData,
            removeData: removeData
        };

        return dataFunctions;

        function getData() {
            return this.data;
        }

        function setData(param, value) {
            this.data[param] = value;
            rs.$broadcast('data_added');
        }

        function removeData(param) {
            delete this.data[param];
            rs.$broadcast('data_removed');
        }
    }
};