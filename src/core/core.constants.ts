/// <reference path="../../typings/tsd.d.ts" />
/* global _ */

module app.core {
    'use strict';

    angular
        .module('app.core')
        .constant('_', _)
        .constant('api', 'http://10.209.52.245:9090')
        .constant('years', '/years')
        .constant('linkages', '/linkages')
        .constant('linkagesByYear', '/linkages/years')
        .constant('completeTransactions', '/transactionsAndAmendments')
        .constant('transactionDetails', '/transactiondetails')
        .constant('allTransactions', '/allTransactions')
        .constant('transactions', '/transactions')
        .constant('scenarios', '/scenarios')
        .constant('steps', '/steps')
        .constant('unlinkedTransactions', '/unlinkedtransactions');
};
