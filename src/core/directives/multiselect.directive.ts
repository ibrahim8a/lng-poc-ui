/// <reference path="../../../typings/tsd.d.ts" />

module app.core {
    'use strict';

    function directiveFunction(): ng.IDirective {

        var directive = {
            link: link,
            restrict: 'E',
            templateUrl: 'core/directives/dropdown.html',
            scope: {
                header: '=',
                optionData: '=',
                selectedEvents: '=',
                singleSelect: '='
            },
            controller: 'multiselectDropdownController',
            controllerAs: 'vm'
        };

        return directive;

        function link(scope, element, attrs) {}
    }


    // ----- ControllerFunction -----

    interface iControllerfunction { };

    class ControllerFunction implements iControllerfunction {
        static $inject = ['$scope', '$element'];

        headerText: string = '';
        isObjectArray: boolean = false;
        selections: Array<any> = [];

        constructor(private scope, private elm) {
            this.scope.headerText = this.scope.header;

            let options = this.scope.optionData;

            if(!options || options.length < 1) {
                return;
            }

            if(typeof options[0] === 'object') {
                this.isObjectArray = true;
            }
        };

        createSelectList = ($evt): void => {
            let currElm: HTMLElement = $evt.currentTarget;
            let span = currElm.querySelector('span');

            if (!this.scope.singleSelect) {
                window.event.stopPropagation();

                if (span.classList.contains('glyphicon-unchecked')) {
                    span.classList.remove('glyphicon-unchecked');
                    span.classList.add('glyphicon-ok');
                } else {
                    span.classList.remove('glyphicon-ok');
                    span.classList.add('glyphicon-unchecked');
                }

                this.getSelectedOptions();
            } else {
                if (this.isObjectArray) {
                    this.selections = [{
                        key: currElm.dataset['key'],
                        value: currElm.textContent.trim()
                    }];
                } else {
                    this.selections = [currElm.textContent.trim()];
                }
            }

            this.updateHeaderText(this.selections);

            this.scope.selectedEvents(this.selections);
        };

        getSelectedOptions = (): void => {
            let elm = this.elm[0];
            let lists = elm.querySelectorAll('.search-options');
            let listLength = lists.length;
            let selections = [];

            for (let inc = 0; inc < listLength; inc++) {
                var anchor = lists[inc].querySelector('a');
                var span = anchor.querySelector('span');

                if (span.classList.contains('glyphicon-ok')) {
                    if (this.isObjectArray) {
                        selections.push({
                            key: anchor.dataset.key,
                            value: anchor.textContent.trim()
                        });
                    } else {
                        selections.push(anchor.textContent.trim());
                    }
                }
            }

            this.selections = selections;
        };

        getSelected = (option: any): boolean => {
            let self = this;
            let found = false;

            if (typeof option === 'string') {
                found = self.selections.indexOf(option) > -1;
            } else {
                let length = self.selections.length;

                for (let inc = 0; inc < length; inc++) {
                    if (self.selections[inc].value === option.value) {
                        found = true;
                    }
                }
            }

            return found;
        };

        updateHeaderText = (data: Array<any>): void => {
            if (!data) {
                return;
            }

            let length = data.length;

            if (data.length === 0) {
                this.scope.headerText = this.scope.header;
            } else if (length === 1) {
                if (typeof data[0] === 'object') {
                    this.scope.headerText = data[0]['value'];
                } else {
                    this.scope.headerText = data[0];
                }
            } else if (length === this.scope.optionData.length) {
                this.scope.headerText = 'All items selected';
            } else {
                this.scope.headerText = length + ' items selected';
            }
        };
    }

    angular.module('app.core')
        .directive('multiselectDropdown', directiveFunction)
        .controller('multiselectDropdownController', ControllerFunction);
};
