/// <reference path="../../../typings/tsd.d.ts" />

module app.core {
    'use strict';

    function directiveFunction($window): ng.IDirective {

        var directive = {
            link: link,
            restrict: 'E',
            scope: {
                type: '@',
                width: '@',
                height: '@',
                data: '=',
                name: '@'
            }
        };
        return directive;

        function link(scope, element, attrs) {
              scope.$watch('data', function() {
                var Highcharts = $window.Highcharts;
                var options = {
                    chart: {
                        zoomType: 'x',
                        height: scope.height,
                        width: scope.width
                    },
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        title: {
                            text: 'PNL'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    title: {
                        text: 'P&L change over time'
                    },
                    plotOptions: {
                        area: {
                            size: '100%',
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                    [0, Highcharts.getOptions().colors[0]],
                                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                ]
                            },
                            marker: {
                                radius: 2
                            },
                            lineWidth: 1,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            threshold: null
                        }
                    },

                    series: [{
                        type: scope.type,
                        data: scope.data,
                        name: scope.name
                    }]
                };
                element.highcharts(options);
            });
        }
    }

    angular.module('app.core')
        .directive('highcharts', directiveFunction);
};
