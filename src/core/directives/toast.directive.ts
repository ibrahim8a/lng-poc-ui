/// <reference path="../../../typings/tsd.d.ts" />

module app.core {
    'use strict';

    function directiveFunction(): ng.IDirective {

        var directive = {
            link: link,
            restrict: 'E',
            templateUrl: 'core/directives/toast.html',
            scope: {
                type: '=',
                title: '=',
                message: '=',
                duration: '=',
                shown: '='
            },
            controller: 'toastController',
            controllerAs: 'vm'
        };

        return directive;

        function link(scope, element, attrs) {}
    }


    // ----- ControllerFunction -----

    interface iControllerfunction { };

    class ControllerFunction implements iControllerfunction {
        static $inject = ['$scope', '$timeout'];

        constructor(private scope, private timeout) {};

        show = (duration?: number) => {
            let self = this;

            if(duration) {
                self.timeout(function() {
                    self.scope.shown = false;
                }, duration);
            } else {
                self.scope.shown = true;
            }
        };

        hide = () => {
            this.scope.shown = false;
        };
    }

    angular.module('app.core')
        .directive('uiToast', directiveFunction)
        .controller('toastController', ControllerFunction);
};
