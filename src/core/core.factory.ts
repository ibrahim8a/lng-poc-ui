/// <reference path="../../typings/tsd.d.ts" />

module app.core {
    'use strict';

    angular
        .module('app.core')
        .factory('CoreFactory', CoreFactory);

    CoreFactory.$inject = ['$http', '$q'];

    /* @ngInject */
    function CoreFactory($http, $q) {
        // var deferred = $q.defer();

        var services = {
            get: get,
            set: set,
            update: update,
            remove: remove
        };

        return services;

        function get(url) {
            let deferred = $q.defer();

            $http.get(url)
                .success(function(response) {
                    deferred.resolve(response);
                })
                .error(function(response) {
                    deferred.reject(response);
                });

            return deferred.promise;
        }

        function set(url, data) {
            let deferred = $q.defer();

            $http.post(url, data)
                .success(function(response) {
                    deferred.resolve(response);
                })
                .error(function(response) {
                    deferred.reject(response);
                });

            return deferred.promise;
        }

        function update(url, data) {
            let deferred = $q.defer();

            $http.put(url, data)
                .success(function(response) {
                    deferred.resolve(response);
                })
                .error(function(response) {
                    deferred.reject(response);
                });

            return deferred.promise;
        }

        function remove(url) {
            let deferred = $q.defer();

            var req = {
                method: 'DELETE',
                url: url
            };

            $http(req)
                .success(function(response) {
                    deferred.resolve(response);
                })
                .error(function(response) {
                    deferred.reject(response);
                });

            return deferred.promise;
        }
    }
};