/// <reference path="../../typings/tsd.d.ts" />

module app.core {
    'use strict';

    /* @ngInject */
    class configFunction {
        static $inject = ['$locationProvider', '$stateProvider', '$urlRouterProvider'];

        constructor($locationProvider, $stateProvider, $urlRouterProvider) {
            $locationProvider.html5Mode(true);

            $urlRouterProvider.otherwise('/');

            $stateProvider
                .state('portfolio', {
                    url: '/',
                    templateUrl: 'components/portfolio-overview/portfolio-overview.html',
                    controller: function($scope) {
                        $scope.page = 'portfolio'
                    }
                })
                .state('portfolio_overview', {
                    url: '/portfolio_overview',
                    templateUrl: 'components/portfolio-overview/portfolio-overview.html',
                    controller: function($scope) {
                        $scope.page = 'portfolio'
                    }
                })
                .state('step_viewer', {
                    url: '/step_viewer',
                    templateUrl: 'components/portfolio-overview/portfolio-overview.html',
                    controller: function($scope) {
                        $scope.page = 'step'
                    }
                })
                .state('linkage_details', {
                    url: '/linkage_details/:id',
                    template: '<tmpl-linkagedetails class="page"></tmpl-linkagedetails>'
                })
                .state('transaction_details', {
                    url: '/transaction_details/:id',
                    template: '<tmpl-linkagedetails class="page"></tmpl-linkagedetails>'
                })
                .state('portfolio_updates', {
                    url: '/portfolio_updates',
                    template: '<tmpl-portfolioupdates class="page"></tmpl-portfolioupdates>'
                })
                .state('scenario_details', {
                    url: '/scenario_details/:id',
                    template: '<tmpl-scenariodetails class="page"></tmpl-scenariodetails>'
                })
        };
    }

    angular.module('app.core')
        .config(configFunction);
};
