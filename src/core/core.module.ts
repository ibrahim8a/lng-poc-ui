/// <reference path="../../typings/tsd.d.ts" />

module app.core {
    'use strict';

    angular.module('app.core', [
        'ngSanitize',

        // Our reusable framework
        // Removing as it might not be useful for production ready apps
        // 'fw.exception', 'fw.logger',

        // 3rd Party modules
        'ui.router',
        'ui.grid',
        'pikaday',
        'ang-drag-drop',
        'html5.sortable'
    ]);
};
